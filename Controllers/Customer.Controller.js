const createError = require("http-errors");
const mongoose = require("mongoose");
const { v4: uuidv4 } = require("uuid");
const Customer = require("../Models/Customer.model");

module.exports = {
  getAllCustomers: async (req, res, next) => {
    let customerIds = [];
    if (req.query.id) {
      customerIds = req.query.id.split(",");
    }

    try {
      let results = [];
      if (customerIds.length > 0) {
        results = await Promise.all(
          customerIds.map(async (id) => {
            return await Customer.findById(id);
          })
        );
      } else {
        results = await Customer.find();
      }

      res.send(results);
    } catch (error) {
      console.log(error.message);
    }
  },

  createNewCustomer: async (req, res, next) => {
    try {
      const customer = new Customer(req.body);
      // create unique API key
      customer.apiKey = uuidv4().replaceAll("-", "");
      // TODO: hash customer password
      const result = await customer.save();
      res.send(result);
    } catch (error) {
      console.log(error.message);
      if (error.name === "ValidationError") {
        next(createError(422, error.message));
        return;
      }
      next(error);
    }
  },
};
