const mongoose = require("mongoose");
const helper = require("../utils/helper");

const AutoIncrement = require("mongoose-sequence")(mongoose);

const customerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      required: "Email address is required",
      validate: [helper.validateEmail, "Please fill a valid email address"],
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        "Please fill a valid email address",
      ],
    },
    accountNo: {
      type: Number,
    },
    username: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
      select: false,
    },
    orders: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Order",
      },
    ],
    bills: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Bill",
      },
    ],
    apiKey: {
      type: String,
      unique: true,
      required: true,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

customerSchema.plugin(AutoIncrement, {
  inc_field: "accountNo",
  start_seq: 1000,
  inc_amount: 1,
});

customerSchema.virtual("accountNumber").get(function () {
  return helper.pad(this.accountNo, 8);
});

module.exports =
  mongoose.models.Customer || mongoose.model("Customer", customerSchema);
