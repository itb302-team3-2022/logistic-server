
const Customer = require("../Models/Customer.model");
const Order = require("../Models/Order.model");
const helper = require("../utils/helper");

const _ = require("lodash");

const genTrackId = (AccountNo) => {
  const timestampStr = (Date.now() + "").slice(7);
  const seed = _.random(1, 100);
  return "SL" + helper.pad(AccountNo, 5) + timestampStr + helper.pad(seed, 3);
};

module.exports = {
  getAllShipments: async (req, res, next) => {
    let trackIds = [];
    if (req.query.tid) {
      trackIds = req.query.tid.split(",");
    }
    let results = [];

    try {
      if (trackIds.length > 0) {
        results = await Promise.all(
          trackIds.map(async (tid) => {
            return await Order.findOne({ trackId: tid }).populate("customer");
          })
        );
      } else {
        results = await Order.find().populate("customer");
      }

      // clearing
      results = results.filter((r) => r !== null);

      if (results.length === 0) {
        throw new Error("shipment(s) record not exist");
      }

      // build response
      results = results.map(
        ({
          shipFrom,
          shipTo,
          pickUp,
          parcels,
          shipmentTimeline,
          shipmentStatusType,
          customer,
          trackId,
          charge
        }) => ({
          charge,
          trackId,
          shipFrom,
          shipTo,
          parcels,
          pickUp,
          pickUp,
          shipmentTimeline,
          shipmentStatusType,
          customer: {
            companyName: customer.name,
            accountNumber: customer.accountNumber,
          },
        })
      );

      res.send(results);
    } catch (error) {
      next(error);
    }
  },

  createNewShipment: async (req, res, next) => {
    const { body } = req;

    try {
      // get customer from db
      const apiKey = body.apiKey;
      if (!apiKey) {
        throw Error("please provide api key");
      }

      const customer = await Customer.findOne({ apiKey });

      if (!customer) {
        throw Error("customer not exist");
      }

      const _newOrder = await Order(req.body);
      _newOrder.customer = customer._id;
      _newOrder.charge = 0;
      _newOrder.trackId = genTrackId(customer.accountNo);
      _newOrder.shipmentStatusType = "pending";
      _newOrder.shipmentTimeline = [
        {
          time: Date.now(),
          title: "Order received",
          message: "We received the shipment request",
          eventStatus: "pending",
        },
      ];

      // create new order
      const newOrder = await _newOrder.save();
      // insert newOrder to customer db
      customer.orders = [...customer.orders, newOrder._id];
      await customer.save();

      res.send(newOrder);
    } catch (err) {
      next(err);
    }
  },
};
