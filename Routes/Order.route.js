const express = require("express");
const router = express.Router();

const OrderController = require("../Controllers/Order.Controller");

//Get a list of all orders
router.get("/", OrderController.getAllOrders);

//Update a order by order id or 
router.put("/:id", OrderController.updateOrderById);

//Update a order charge by order id
router.put("/charge/:id", OrderController.updateOrderChargeById);

module.exports = router;
