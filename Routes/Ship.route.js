const express = require("express");
const router = express.Router();

const ShipController = require("../Controllers/Ship.Controller");

//Get a list of all products
router.get("/", ShipController.getAllShipments);

//Create a new shipment
router.post("/", ShipController.createNewShipment);

module.exports = router;
