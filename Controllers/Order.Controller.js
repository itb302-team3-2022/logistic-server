
const Order = require("../Models/Order.model");

module.exports = {
  getAllOrders: async (req, res, next) => {
    let orderIds = [];
    const {
      query: { id, status },
    } = req;

    if (status) {
      const result = await Order.find({ statusType: status });
      return res.send(result);
    }

    if (id) {
      orderIds = id.split(",");
    }

    try {
      let results = [];
      if (orderIds.length > 0) {
        results = await Promise.all(
          orderIds.map(async (id) => {
            return await Order.findById(id)
              .sort({ createdAt: -1 })
              .populate("customer");
          })
        );
      } else {
        results = await Order.find()
          .sort({ createdAt: -1 })
          .populate("customer");
      }

      res.send(results);
    } catch (error) {
      next(error);
    }
  },

  updateOrderChargeById: async (req, res, next) => {
    const { body, params } = req;

    try {
      if (!body.charge) {
        throw Error("Please input the charge amount");
      }

      // param can be order id or track id
      let order = await Order.findOne({ trackId: params.id });
      if (order.length === 0) {
        order = await Order.findById(params.id);
      }

      if (order.length === 0) {
        throw Error("order not found");
      }

      order.charge = body.charge;
      const updatedOrder = await order.save();
      return res.send(updatedOrder);

    } catch (err) {
      next(err);
    }
  },

  updateOrderById: async (req, res, next) => {
    const { body, params } = req;

    try {
      // param can be order id or track id
      let order = await Order.findOne({ trackId: params.id });
      if (order.length === 0) {
        order = await Order.findById(params.id);
      }

      if (order.length === 0) {
        throw Error("order not found");
      }

      if (body.status) {
        order.statusType = body.status;
      }

      if (body.charge) {
        order.charge = body.charge;
      }


      if (body.event) {
        order.shipmentTimeline.push({
          time: Date.now(),
          title: body.event.title,
          message: body.event.message,
          eventStatus: body.event.eventStatus,
        });
      }

      order.shipmentStatusType = body.event.eventStatus;

      if (body.status || body.shipmentStatus || body.event) {
        const updatedOrder = await order.save();
        return res.send(updatedOrder);
      }

      res.send(order);
    } catch (err) {
      next(err);
    }
  },
};
