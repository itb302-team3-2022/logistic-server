const express = require("express");
const router = express.Router();

const CustomerController = require("../Controllers/Customer.Controller");

//Get a list of all customers
router.get("/", CustomerController.getAllCustomers);

//Create a new customer
router.post("/", CustomerController.createNewCustomer);

module.exports = router;
