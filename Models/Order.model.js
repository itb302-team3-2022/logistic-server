const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
  {
    customer: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      ref: "Customer",
    },
    shipFrom: {
      name: String,
      phone: String,
      phone2: String,
      address: String,
    },
    shipTo: {
      name: String,
      phone: String,
      phone2: String,
      address: String,
    },
    parcels: [
      {
        refId: String,
        weight: {
          value: Number,
          unit: {
            type: String,
            enum: ["kg", "lb", "g"],
            default: "kg",
          },
        },
        dimension: {
          depth: Number,
          width: Number,
          height: Number,
          unit: {
            type: String,
            enum: ["cm", "ft", "inch", "m"],
            default: "cm",
          },
        },
        description: String,
        insuredValue: {
          amount: Number,
          currency: String,
        },
      },
    ],
    pickUp: {
      startTime: Date,
      endTime: Date,
    },
    trackId: {
      type: String,
      required: true,
    },
    statusType: {
      type: String,
      enum: ["new", "paused", "rejected", "processing", "completed", "canceled"],
      default: "new",
    },
    shipmentStatusType: {
      type: String,
      enum: ["pending", "preparing", "picking", "shipping", "shipped", "canceled"],
    },
    shipmentTimeline: [
      {
        time: Date,
        title: String,
        message: String,
        eventStatus: {
          type: String,
          enum: ["pending", "preparing", "picking", "shipping", "shipped", "canceled"],
        },
      },
    ],
    charge: {
      type: Number,
      required: true,
    },
    invoiceRef: mongoose.SchemaTypes.ObjectId,
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.models.Order || mongoose.model("Order", orderSchema);
