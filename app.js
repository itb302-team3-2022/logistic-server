const express = require("express");
const createError = require("http-errors");
const dotenv = require("dotenv").config();
const cors = require('cors')

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Initialize DB
require("./initDB")();

// const ProductRoute = require("./Routes/Product.route");
const CustomerRoute = require("./Routes/Customer.route");
const OrderRoute = require("./Routes/Order.route");
const ShipRoute = require("./Routes/Ship.route");
app.get("/", (req, res) => {
  res.sendString("welcome to logistic API ");
});

// app.use("/api/products", ProductRoute);
app.use("/api/customers", CustomerRoute);
app.use("/api/orders", OrderRoute);
app.use("/api/shipment", ShipRoute);
// app.use("/api/ship", ShipRoute);

//404 handler and pass to error handler
app.use((req, res, next) => {
  /*
  const err = new Error('Not found');
  err.status = 404;
  next(err);
  */
  next(createError(404, "Not found"));
});

//Error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      status: err.status || 500,
      message: err.message,
    },
  });
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log("Server started on port " + PORT + "...");
});
